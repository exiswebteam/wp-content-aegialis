<?php

/*-----------------------------------------------------------------------
  Availability Account text
-----------------------------------------------------------------------*/
add_filter ( 'woocommerce_account_menu_items', 'exis_account_availability' );

function exis_account_availability( $menu_links ){

	// we will hook "anyuniquetext123" later
	$new = array( 'edit-my-availability' => 'Edit Availability' );

	// or in case you need 2 links
	// $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );

	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 1, true )
	+ $new
	+ array_slice( $menu_links, 1, NULL, true );


	return $menu_links;
}

/*-----------------------------------------------------------------------
  Availability Account Link
-----------------------------------------------------------------------*/
add_filter( 'woocommerce_get_endpoint_url', 'exis_account_availability_hook_endpoint', 10, 4 );

function exis_account_availability_hook_endpoint( $url, $endpoint, $value, $permalink ){

	if( $endpoint === 'edit-my-availability' ) {

		// ok, here is the place for your custom URL, it could be external
		$url = site_url();

	}
	return $url;

}
